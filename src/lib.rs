mod stupid_stack;
mod stack_error;
mod stack_operation;

pub use crate::stupid_stack::StupidStack;
pub use crate::stack_error::StackError;
pub(crate) use crate::stack_operation::StackOperation;


#[test]
fn stack_works() {
  let mut stack = StupidStack::<[u8; 4]>::new();
  let first = [1, 2, 3, 4];
  let second = [5, 6, 7, 8];
  let third = [9, 10, 11, 12];

  stack.push(first).unwrap();
  stack.push(second).unwrap();
  assert_eq!(stack.pop().unwrap(), second);
  stack.push(third).unwrap();
  assert_eq!(stack.pop().unwrap(), third);
  assert_eq!(stack.pop().unwrap(), first);
}

#[test]
#[should_panic]
fn stack_underflows() {
  let mut stack = StupidStack::<[u8; 64]>::new();
  stack.pop().unwrap();
}

// NOTE: This doesn't actually work, the test still fails when the thread overflows.
// #[test]
// #[should_panic]
// fn stack_overflows() {
//   let mut stack = StupidStack::<[u8; 64]>::new();
//
//   for _i in 0..10000000 {
//     if let Err(_err) = stack.push([0; 64]) {
//       // Not technically reachable since stack overflows bring down
//       // the whole program
//     }
//   }
// }