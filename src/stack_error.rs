use std::fmt::{Debug, Display, Formatter};
use std::sync::mpsc::{RecvError, SendError};
use crate::StackOperation;

pub enum StackError<T: Send> {
  SendFailure(SendError<StackOperation<T>>),
  ReceiveFailure(RecvError)
}

impl<T: Send> Debug for StackError<T> {
  fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
    match self {
      StackError::SendFailure(err) => {
        write!(f, "SendFailure({})", err)
      },
      StackError::ReceiveFailure(err) => {
        write!(f, "ReceiveFailure({:?})", err)
      }
    }
  }
}

impl<T: Send> Display for StackError<T> {
  fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
    write!(f, "{:?}", self)
  }
}

impl<T: Send> From<SendError<StackOperation<T>>> for StackError<T> {
  fn from(error: SendError<StackOperation<T>>) -> Self {
    StackError::SendFailure(error)
  }
}

impl<T: Send> From<RecvError> for StackError<T> {
  fn from(error: RecvError) -> Self {
    StackError::ReceiveFailure(error)
  }
}