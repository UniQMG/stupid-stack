use std::sync::mpsc;
use std::sync::mpsc::{Receiver, Sender};
use std::thread;
use std::thread::JoinHandle;
use crate::{StackError, StackOperation};

pub struct StupidStack<T: Send> {
  _handle: JoinHandle<()>,
  tx: Sender<StackOperation<T>>
}

impl<T: Send + 'static> StupidStack<T> {
  pub fn new() -> Self {
    let (tx, rx) = mpsc::channel();
    let handle = thread::spawn(|| {
      Self::stupid_stack(None, rx);
      println!("Thread exited");
    });
    StupidStack {
      _handle: handle,
      tx
    }
  }

  pub fn push(&mut self, value: T) -> Result<(), StackError<T>> {
    Ok(self.tx.send(StackOperation::Push(value))?)
  }

  pub fn pop(&mut self) -> Result<T, StackError<T>> {
    let (tx, rx) = mpsc::channel();
    self.tx.send(StackOperation::Pop(tx))?;
    Ok(rx.recv()?)
  }

  fn stupid_stack(value: Option<T>, mut rx: Receiver<StackOperation<T>>) -> Receiver<StackOperation<T>> {
    loop {
      match rx.recv() {
        Ok(StackOperation::Push(value)) => {
          println!("Push value");
          rx = Self::stupid_stack(Some(value), rx);
        },
        Ok(StackOperation::Pop(sender)) => {
          println!("Pop value");
          match value {
            Some(value) => {
              sender.send(value).expect("Receiver hung up");
              return rx;
            },
            None => panic!("Stack underflow")
          }
        },
        // Further calls will unwind stack until it exits
        Err(_err) => return rx
      }
    }
  }
}