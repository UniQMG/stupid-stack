use std::sync::mpsc::Sender;

pub enum StackOperation<T: Send> {
  Push(T),
  Pop(Sender<T>)
}